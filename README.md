# Mastering git course: Homework 1

This repository is my attempt to solve homework 1 of Mastering Git course.

Theme of this repository: Quadball (formerly known as quidditch)


Note: For lecturer's convenience I deleted all lines from log which were not directly related to directly solve hw1.
These were mostly commands which were used in my work in other courses. All commands used during solving of homework are present
in log.
