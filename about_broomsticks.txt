Broomsticks

Broomsticks are one of the forms of transportation for wizards and witches, as well as being used for playing Quidditch. The three most prominent broomsticks in the books are the Nimbus 2000, Nimbus 2001, and the Firebolt, both of which have been produced as merchandise by Warner Bros.
In game of quadball are used broomsticks made of PVC. 

The Nimbus is introduced as one of the best broomsticks in the wizarding world. Harry receives a Nimbus 2000 in Philosopher's Stone so that he can play for Gryffindor house. In Chamber of Secrets, Lucius Malfoy buys a full set of the more advanced Nimbus 2001s for the Slytherin team as a bribe, so they would choose his son Draco as Seeker that year.

The Firebolt later supersedes the Nimbus as the fastest and one of the most expensive racing brooms in existence. Harry receives a Firebolt model from his godfather, Sirius Black, after his Nimbus 2000 is destroyed during a Quidditch match in Prisoner of Azkaban. In Goblet of Fire, Harry uses his Firebolt to escape the Hungarian Horntail during the Triwizard Tournament.

